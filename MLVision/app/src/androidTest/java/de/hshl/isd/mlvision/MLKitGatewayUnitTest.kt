package de.hshl.isd.mlvision

import android.graphics.BitmapFactory
import androidx.test.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel
import com.google.firebase.ml.vision.objects.FirebaseVisionObject
import com.google.firebase.ml.vision.text.FirebaseVisionText
import de.hshl.isd.basiccleanarch.Response
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.fail
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class MLKitGatewayUnitTest {
    val gateway = MLKitGateway()
    val ctx = InstrumentationRegistry.getTargetContext()
    val assetManager = ctx.resources.assets

    @Test
    fun testRecognizeText() {
        val stream = assetManager.open("image_has_text.jpg")
        val bitmap = BitmapFactory.decodeStream(stream)
        runBlocking {
            val result = gateway.recognizeText(bitmap)
            when (result) {
                is Response.Success<*> -> {
                    val visionText = result.value as FirebaseVisionText
                    assertEquals(visionText.text, "Gallery 1\nPlease Enter")
                }
                is Response.Failure -> {
                    fail(result.toString())
                }
            }
        }
    }

    @Test
    fun testDetectBarcode() {
        val stream = assetManager.open("barcode_128.png")
        val bitmap = BitmapFactory.decodeStream(stream)
        runBlocking {
            val result = gateway.detectBarcode(bitmap)
            when (result) {
                is Response.Success<*> -> {
                    val visionBarcode = result.value as List<FirebaseVisionBarcode>
                    assertEquals(visionBarcode.first().displayValue, "this is an encoded word")
                }
                is Response.Failure -> {
                    fail(result.toString())
                }
            }
        }
    }

    @Test
    fun testLabelImage() {
        val stream = assetManager.open("beach.jpg")
        val bitmap = BitmapFactory.decodeStream(stream)
        runBlocking {
            val result = gateway.labelImage(bitmap)
            when (result) {
                is Response.Success<*> -> {
                    val imageLabels = result.value as List<FirebaseVisionImageLabel>
                    val labelTexts = imageLabels.map { it.text }
                    assert(labelTexts.contains("Beach"))
                    assert(labelTexts.contains("Rock"))
                    assert(labelTexts.contains("Sky"))
                    assert(labelTexts.contains("Cliff"))
                    assert(labelTexts.contains("Sand"))
                }
                is Response.Failure -> {
                    fail(result.toString())
                }
            }
        }
    }

    @Test
    fun testDetectObject() {
        val stream = assetManager.open("liberty.jpg")
        val bitmap = BitmapFactory.decodeStream(stream)
        runBlocking {
            val result = gateway.detectObjects(bitmap)
            when (result) {
                is Response.Success<*> -> {
                    val objects = result.value as List<FirebaseVisionObject>
                    assert(!objects.isEmpty())
                }
                is Response.Failure -> {
                    fail(result.toString())
                }
            }
        }
    }
}
