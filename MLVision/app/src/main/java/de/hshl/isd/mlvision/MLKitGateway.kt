package de.hshl.isd.mlvision

import android.graphics.Bitmap
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel
import com.google.firebase.ml.vision.objects.FirebaseVisionObject
import com.google.firebase.ml.vision.objects.FirebaseVisionObjectDetectorOptions
import com.google.firebase.ml.vision.text.FirebaseVisionText
import de.hshl.isd.basiccleanarch.Response
import kotlinx.coroutines.tasks.await

class MLKitGateway {

    val vision = FirebaseVision.getInstance()

    suspend fun recognizeText(bitmap: Bitmap): Response {
        val detector = vision.onDeviceTextRecognizer
        try {
            val image = FirebaseVisionImage.fromBitmap(bitmap)
            val result = detector.processImage(image).await()
            return Response.Success<FirebaseVisionText>(result)
        } catch (e: Exception) {
            return Response.Failure(e)
        }
    }

    suspend fun detectBarcode(bitmap: Bitmap): Response {
        val options = FirebaseVisionBarcodeDetectorOptions.Builder()
            .setBarcodeFormats(FirebaseVisionBarcode.FORMAT_ALL_FORMATS)
            .build()
        val detector = vision
            .getVisionBarcodeDetector(options)
        try {
            val image = FirebaseVisionImage.fromBitmap(bitmap)
            val result = detector.detectInImage(image).await()
            return Response.Success<List<FirebaseVisionBarcode>>(result)
        } catch (e: Exception) {
            return Response.Failure(e)
        }
    }

    suspend fun labelImage(bitmap: Bitmap): Response {
        val labeler = vision.onDeviceImageLabeler
        try {
            val image = FirebaseVisionImage.fromBitmap(bitmap)
            val result = labeler.processImage(image).await()
            return Response.Success<List<FirebaseVisionImageLabel>>(result)
        } catch (e: Exception) {
            return Response.Failure(e)
        }
    }

    suspend fun detectObjects(bitmap: Bitmap): Response {
        val options = FirebaseVisionObjectDetectorOptions.Builder()
            .setDetectorMode(FirebaseVisionObjectDetectorOptions.SINGLE_IMAGE_MODE)
            .enableMultipleObjects()
            .enableClassification()
            .build()

        val detector = vision.getOnDeviceObjectDetector(options)

        try {
            val image = FirebaseVisionImage.fromBitmap(bitmap)
            val result = detector.processImage(image).await()
            return Response.Success<List<FirebaseVisionObject>>(result)
        } catch (e: Exception) {
            return Response.Failure(e)
        }
    }

}