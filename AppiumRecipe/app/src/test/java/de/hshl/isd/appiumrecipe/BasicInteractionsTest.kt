package de.hshl.isd.appiumrecipe

import io.appium.java_client.android.Activity
import io.appium.java_client.android.AndroidElement
import org.junit.Assert
import org.junit.Test

class BasicInteractionsTest : BaseTest() {

    private val MAIN_ACTIVITY = ".MainActivity"
    private val PACKAGE = "de.hshl.isd.appiumrecipe"

    @Test
    fun testSendKeys() {
        driver!!.startActivity(Activity(PACKAGE, MAIN_ACTIVITY))

        val editText = driver!!.findElementById("editText") as AndroidElement
        editText.sendKeys("Hello world!")

        val button = driver!!.findElementById("button") as AndroidElement
        button.click()

        val resultText = driver!!.findElementById("resultTextView")
        Assert.assertEquals(resultText.text, "Hello world!")

        captureScreenShot()
    }
}