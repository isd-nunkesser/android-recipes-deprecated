package de.hshl.isd.appiumrecipe

import io.appium.java_client.android.AndroidDriver
import org.apache.commons.io.FileUtils
import org.junit.AfterClass
import org.junit.BeforeClass
import org.openqa.selenium.OutputType
import org.openqa.selenium.WebElement
import org.openqa.selenium.remote.DesiredCapabilities
import java.io.File
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

abstract class BaseTest {
    companion object {

        var driver: AndroidDriver<WebElement>? = null
        val screenshotFolder = "screenshots"

        val serviceUrl: URL?
            get() {
                try {
                    return URL("http://0.0.0.0:4723/wd/hub")
                } catch (e: MalformedURLException) {
                    return null
                }

            }

        @BeforeClass
        @JvmStatic
        @Throws(IOException::class)
        fun setUp() {
            val classpathRoot = File(System.getProperty("user.dir")!!)
            val appDir = File(classpathRoot, "./build/outputs/apk/debug/")
            var app = File(appDir.canonicalPath, "app-debug.apk")
            val capabilities = DesiredCapabilities()
            capabilities.setCapability("deviceName", "Android Emulator")
            capabilities.setCapability("app", app.absolutePath)
            driver = AndroidDriver(BaseTest.serviceUrl!!, capabilities)
        }

        @Throws(IOException::class)
        fun captureScreenShot() {
            val file = driver!!.getScreenshotAs<File>(OutputType.FILE)
            val df = SimpleDateFormat("yyyy-MM-dd__HH_mm_ss")
            File(screenshotFolder).mkdir()
            val file_name = df.format(Date()) + ".png"
            FileUtils.copyFile(file, File(screenshotFolder + "/" + file_name))
        }

        @AfterClass
        @JvmStatic
        fun tearDown() {
            driver!!.quit()
        }
    }
}

