package de.hshl.isd.mvvmrecipecompose

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.state
import androidx.ui.core.Modifier
import androidx.ui.foundation.Text
import androidx.ui.foundation.TextField
import androidx.ui.core.setContent
import androidx.ui.foundation.TextFieldValue
import androidx.ui.layout.*
import androidx.ui.material.Button
import androidx.ui.material.MaterialTheme
import androidx.ui.material.Scaffold
import androidx.ui.material.TopAppBar
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp

class MainActivity : AppCompatActivity() {
    private val viewModel = MainViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                MainContent(viewModel)
            }
        }
    }
}

@Composable
fun MainContent(viewModel: MainViewModel) {
    Scaffold(
        topAppBar = {
            TopAppBar(title = { Text(text = "MVVM") })
        },
        bodyContent = {
        Column(verticalArrangement = Arrangement.Center) {
            val forenameTextField = state { TextFieldValue("Forename") }
            TextField(value = forenameTextField.value,
                modifier = Modifier.padding(8.dp),
                onValueChange = {
                    forenameTextField.value = it
                    viewModel.forename = it.text
                })
            val surnameTextField = state { TextFieldValue("Surname") }
            TextField(value = surnameTextField.value,
                modifier = Modifier.padding(8.dp),
                onValueChange = {
                    surnameTextField.value = it
                    viewModel.surname = it.text
                })
            Row(modifier = Modifier.padding(8.dp)) {
                Text(text = "Forename")
                Text(
                    text = viewModel.forename,
                    modifier = Modifier.padding(8.dp)
                )
            }
            Row(modifier = Modifier.padding(8.dp)) {
                Text(text = "Surname")
                Text(
                    text = viewModel.surname,
                    modifier = Modifier.padding(8.dp)
                )

            }
            Row(modifier = Modifier.padding(8.dp)) {
                Text(text = "Complete Name")
                Text(
                    text = viewModel.name,
                    modifier = Modifier.padding(8.dp)
                )

            }
            Button(modifier = Modifier.padding(8.dp),
                onClick = { viewModel.reset() }) {
                Text("Reset")
            }
        }
    })
}

@Preview
@Composable
fun DefaultPreview() {
    MaterialTheme {
        MainContent(MainViewModel())
    }
}
