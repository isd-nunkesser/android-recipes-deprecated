package de.hshl.isd.mvvmrecipecompose

import androidx.compose.Model

@Model
class MainViewModel {
    var forename: String = ""
    var surname: String = ""
    var name: String = ""
        get() = "${forename} ${surname}"
    fun reset() {
        forename  = ""
        surname = ""
    }
}