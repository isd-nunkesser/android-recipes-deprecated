package de.hshl.isd.httpget

import de.hshl.isd.basiccleanarch.Response
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.fail
import kotlinx.coroutines.runBlocking
import org.junit.Test

class HttpBinGatewayTest {

    @Test
    fun testFetchData() {
        runBlocking {
            val result = HttpBinGateway().fetchData()
            when (result) {
                is Response.Success<*> -> {
                    val model = result.value as HttpRequestModel
                    assertNotNull(model.origin)
                    assertNotNull(model.url)
                }
                is Response.Failure -> {
                    fail(result.error.localizedMessage)
                }
            }
        }
    }
}