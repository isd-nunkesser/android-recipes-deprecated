package de.hshl.isd.httpget

import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Response
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.util.concurrent.CompletableFuture


class GetHttpRequestInteractorTests {

    @Mock
    private var mockPresenter = mock(HttpRequestPresenter::class.java)

    @Mock
    private var mockGateway = mock(HttpBinGateway::class.java)


    private class testDisplayer(val future: CompletableFuture<Int>) :
            Displayer {
        override fun display(result: Response) {
            future.complete(1)
        }

    }

    @Test
    fun testCallDisplayer() {
        val future = CompletableFuture<Int>()

        val model = HttpRequestModel("", "")
        `when`(mockGateway.fetchData()).thenReturn(
                Response.Success<HttpRequestModel>(model))

        `when`(mockPresenter.present(model)).thenReturn("")

        GetHttpRequestInteractor(mockPresenter, mockGateway).execute(null,
                testDisplayer(future))
        assertEquals(1, future.get() as Int)
    }
}