package de.hshl.isd.httpget

import retrofit2.Call
import retrofit2.http.GET

interface HttpBinAPI {
    @GET("get")
    fun getExample(): Call<HttpRequestModel>
}