package de.hshl.isd.httpget

import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Presenter
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class GetHttpRequestInteractor(
        override val presenter: Presenter<HttpRequestModel, String>,
        val gateway: HttpBinGateway) :
            UseCase<Void?, HttpRequestModel, String> {

    override fun execute(request: Void?, displayer: Displayer<String>,
                         requestCode: Int) {
        GlobalScope.launch {
            val result = gateway.fetchData()
            when (result) {
                is Response.Success<*> -> {
                    val entity = result.value as HttpRequestModel
                    val viewModel = presenter.present(entity)
                    displayer.display(viewModel)
                }
                is Response.Failure -> {
                    displayer.display(result.error)
                }
            }
        }
    }

}