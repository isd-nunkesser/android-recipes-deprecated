package de.hshl.isd.httpget

data class HttpRequestModel(val origin: String, val url: String)