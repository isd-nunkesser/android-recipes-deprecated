package de.hshl.isd.httpget

import de.hshl.isd.basiccleanarch.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

open class HttpBinGateway {

    open fun fetchData(): Response {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://httpbin.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        val service = retrofit.create(HttpBinAPI::class.java)

        try {
            val response = service.getExample().execute()
            return Response.Success<HttpRequestModel>(response.body()!!)
        } catch (t: Throwable) {
            return Response.Failure(t)
        }

    }
}