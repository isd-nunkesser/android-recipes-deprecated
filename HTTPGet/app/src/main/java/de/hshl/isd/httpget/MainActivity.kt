package de.hshl.isd.httpget

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import de.hshl.isd.basiccleanarch.Displayer

class MainActivity : AppCompatActivity(), Displayer<String> {

    private val tag = "MainActivity"
    private val interactor
            = GetHttpRequestInteractor(HttpRequestPresenter(), HttpBinGateway())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        interactor.execute(null, displayer = this)
    }

    override fun display(success: String, requestCode: Int) {
        Log.i(tag, success)
    }

    override fun display(error: Throwable) {
        Log.e(tag, error.localizedMessage)
    }

}
