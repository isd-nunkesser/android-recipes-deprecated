package de.hshl.isd.httpget

import de.hshl.isd.basiccleanarch.Presenter

open class HttpRequestPresenter : Presenter<HttpRequestModel, String> {
    override fun present(model: HttpRequestModel): String =
            "Origin: ${model.origin}, Url: ${model.url}"
}