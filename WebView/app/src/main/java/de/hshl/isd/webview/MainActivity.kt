package de.hshl.isd.webview

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import de.hshl.isd.webview.ui.main.MainFragment
import java.net.URL

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance(URL("https://www.hshl.de")))
                    .commitNow()
        }
    }

}
