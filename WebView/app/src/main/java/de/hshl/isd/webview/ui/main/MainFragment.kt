package de.hshl.isd.webview.ui.main

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.hshl.isd.webview.R
import kotlinx.android.synthetic.main.main_fragment.*
import java.net.URL

private const val ARG_URL = "url"

class MainFragment : Fragment() {
    private var url: URL? = null

    companion object {
        @JvmStatic
        fun newInstance(url: URL) =
                MainFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(ARG_URL, url)
                    }
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            url = it.getSerializable(ARG_URL) as URL?
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView.loadUrl(url.toString())
    }

}
